<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="varchar">Kode Barang <?php echo form_error('kode_barang') ?></label>
        <input type="text" class="form-control" readonly name="kode_barang" id="kode_barang" placeholder="Kode Barang" value="<?php echo $kode_barang; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Nama Barang <?php echo form_error('nama_barang') ?></label>
        <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Nama Barang" value="<?php echo $nama_barang; ?>" />
    </div>
    <div class="form-group">
        <label for="int">Harga <?php echo form_error('harga') ?></label>
        <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>" />
    </div>
    <!-- <div class="form-group">
        <label for="int">Foto Barang </label>
        <input type="file" class="form-control" name="foto_barang" />
    </div> -->





    <div class="form-group">

        <input type="hidden" class="form-control" name="stok" id="stok" placeholder="Stok" value="5" />
    </div>
    <input type="hidden" name="id_barang" value="<?php echo $id_barang; ?>" />
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
    <a href="<?php echo site_url('barang') ?>" class="btn btn-default">Cancel</a>
</form>